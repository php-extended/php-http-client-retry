<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-retry library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * RetryConfiguration class file.
 * 
 * This class represents parameters to change the behavior of retry clients.
 * 
 * @author Anastaszor
 */
class RetryConfiguration implements Stringable
{
	
	/**
	 * The number of retry attempts to make before stopping. Defaults to 4.
	 * 
	 * @var integer
	 */
	protected int $_nbRetry = 4;
	
	/**
	 * Whether to retry on empty body for GET requests. Defaults to true.
	 * 
	 * @var boolean
	 */
	protected bool $_retryOnEmptyBody = true;
	
	/**
	 * The default http codes which are used for automatic retry. By default
	 * this contains all the standard http status codes that are used if the
	 * client may have a different response by sending the same request (later).
	 * 
	 * @var array<integer, integer>
	 */
	protected $_retryCodes = [
		408,	// Request Timeout
		500,	// Internal Server Error
		502,	// Bad Gateway
		503,	// Service Unavailable
		504,	// Gateway Timeout
	];
	
	/**
	 * The callback to assert whether a response is acceptable for a given
	 * request.
	 * 
	 * @var ?RetryResponseAcceptabilityCallbackInterface
	 */
	protected ?RetryResponseAcceptabilityCallbackInterface $_acceptabilityCb = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the quantity of max retries.
	 * 
	 * @param integer $quantity
	 * @return RetryConfiguration
	 */
	public function setNbRetries(int $quantity) : RetryConfiguration
	{
		$this->_nbRetry = $quantity;
		if(0 > $this->_nbRetry)
		{
			$this->_nbRetry = 4;
		}
		
		return $this;
	}
	
	/**
	 * Gets the max number of retries.
	 * 
	 * @return integer
	 */
	public function getNbRetries() : int
	{
		return $this->_nbRetry;
	}
	
	/**
	 * Enables the retry for get requests on empty bodies.
	 * 
	 * @return RetryConfiguration
	 */
	public function enableRetryGetOnEmptyBody() : RetryConfiguration
	{
		$this->_retryOnEmptyBody = true;
		
		return $this;
	}
	
	/**
	 * Disables the retry for get requests on empty bodies.
	 * 
	 * @return RetryConfiguration
	 */
	public function disableRetryGetOnEmptyBody() : RetryConfiguration
	{
		$this->_retryOnEmptyBody = false;
		
		return $this;
	}
	
	/**
	 * Gets whether to retry on empty bodies.
	 * 
	 * @return boolean
	 */
	public function isEnabledRetryOnEmptyBody() : bool
	{
		return $this->_retryOnEmptyBody;
	}
	
	/**
	 * Adds a retry code to the retry codes list.
	 * 
	 * @param integer $code
	 * @return RetryConfiguration
	 */
	public function addRetryCode(int $code) : RetryConfiguration
	{
		if(100 <= $code && 600 > $code)
		{
			if(!\in_array($code, $this->_retryCodes, true))
			{
				$this->_retryCodes[] = $code;
				\sort($this->_retryCodes);
			}
		}
		
		return $this;
	}
	
	/**
	 * Removes a retry code from the retry code list.
	 * 
	 * @param integer $code
	 * @return RetryConfiguration
	 */
	public function removeRetryCode(int $code) : RetryConfiguration
	{
		if(\in_array($code, $this->_retryCodes, true))
		{
			$retries = [];
			
			foreach($this->_retryCodes as $retryCode)
			{
				if($retryCode !== $code)
				{
					$retries[] = $retryCode;
				}
			}
			$this->_retryCodes = $retries;
		}
		
		return $this;
	}
	
	/**
	 * Gets whether the given code is in the retry code list.
	 * 
	 * @param integer $code
	 * @return bool
	 */
	public function hasRetryCode(int $code) : bool
	{
		return \in_array($code, $this->_retryCodes, true);
	}
	
	/**
	 * Gets all the retry codes for this configuration.
	 * 
	 * @return array<integer, integer>
	 */
	public function getRetryCodes() : array
	{
		return $this->_retryCodes;
	}
	
	/**
	 * Sets the acceptability callback for responses.
	 * 
	 * @param RetryResponseAcceptabilityCallbackInterface $callback
	 * @return RetryConfiguration
	 */
	public function setAcceptabilityCallback(RetryResponseAcceptabilityCallbackInterface $callback) : RetryConfiguration
	{
		$this->_acceptabilityCb = $callback;
		
		return $this;
	}
	
	/**
	 * Absorbs the given callback with other callbacks.
	 * 
	 * @param RetryResponseAcceptabilityCallbackInterface $callback
	 * @return RetryConfiguration
	 */
	public function mergeAcceptabilityCallback(?RetryResponseAcceptabilityCallbackInterface $callback = null) : RetryConfiguration
	{
		if(null === $callback)
		{
			return $this;
		}
		
		if(null === $this->_acceptabilityCb)
		{
			$this->_acceptabilityCb = $callback;
			
			return $this;
		}
		
		$this->_acceptabilityCb = new RetryMergedAcceptabilityCallback($this->_acceptabilityCb, $callback);
		
		return $this;
	}
	
	/**
	 * Gets the acceptability callback for responses.
	 * 
	 * @return ?RetryResponseAcceptabilityCallbackInterface
	 */
	public function getAcceptabilityCallback() : ?RetryResponseAcceptabilityCallbackInterface
	{
		return $this->_acceptabilityCb;
	}
	
	/**
	 * Gets whether the given status code is acceptable (i.e. we do not need
	 * to retry the request).
	 * 
	 * @param integer $statusCode
	 * @return boolean
	 */
	public function isStatusCodeAcceptable(int $statusCode) : bool
	{
		return !\in_array($statusCode, $this->_retryCodes, true);
	}
	
	/**
	 * Builds a new RetryConfiguration from this configuration and the
	 * other given configuration.
	 * 
	 * @param RetryConfiguration $other
	 * @return RetryConfiguration
	 */
	public function mergeWith(RetryConfiguration $other) : RetryConfiguration
	{
		$configuration = new self();
		
		$configuration->setNbRetries(\max($this->getNbRetries(), $other->getNbRetries()));
		
		foreach($this->getRetryCodes() as $retryCode)
		{
			$configuration->addRetryCode($retryCode);
		}
		
		foreach($other->getRetryCodes() as $retryCode)
		{
			$configuration->addRetryCode($retryCode);
		}
		
		$configuration->mergeAcceptabilityCallback($this->getAcceptabilityCallback());
		$configuration->mergeAcceptabilityCallback($other->getAcceptabilityCallback());
		
		return $configuration;
	}
	
}
