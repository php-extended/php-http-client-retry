<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-retry library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * RetryMergedAcceptabilityCallback class file.
 * 
 * This class is a callback that accepts a request only if both sub-callbacks
 * accepts it also.
 * 
 * @author Anastaszor
 */
class RetryMergedAcceptabilityCallback implements RetryResponseAcceptabilityCallbackInterface
{
	
	/**
	 * The first callback.
	 * 
	 * @var RetryResponseAcceptabilityCallbackInterface
	 */
	protected RetryResponseAcceptabilityCallbackInterface $_firstCallback;
	
	/**
	 * The second callback.
	 * 
	 * @var RetryResponseAcceptabilityCallbackInterface
	 */
	protected RetryResponseAcceptabilityCallbackInterface $_secondCallback;
	
	/**
	 * Builds a new RetryMergedAcceptabilityCallback with the two given callbacks.
	 * 
	 * @param RetryResponseAcceptabilityCallbackInterface $callback1
	 * @param RetryResponseAcceptabilityCallbackInterface $callback2
	 */
	public function __construct(RetryResponseAcceptabilityCallbackInterface $callback1, RetryResponseAcceptabilityCallbackInterface $callback2)
	{
		$this->_firstCallback = $callback1;
		$this->_secondCallback = $callback2;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpClient\RetryResponseAcceptabilityCallbackInterface::isResponseAcceptable()
	 */
	public function isResponseAcceptable(RequestInterface $request, ResponseInterface $response) : bool
	{
		return $this->_firstCallback->isResponseAcceptable($request, $response)
			&& $this->_secondCallback->isResponseAcceptable($request, $response);
	}
	
}
