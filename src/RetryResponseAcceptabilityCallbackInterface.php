<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-retry library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * RetryResponseAcceptabilityCallbackInterface interface file.
 * 
 * This class represents a callback to assert whether a response is acceptable
 * for a given request.
 * 
 * @author Anastaszor
 */
interface RetryResponseAcceptabilityCallbackInterface extends Stringable
{
	
	/**
	 * Asserts that the given response is acceptable in front of the given
	 * request.
	 * 
	 * @param RequestInterface $request
	 * @param ResponseInterface $response
	 * @return boolean true if the response is acceptable and should be kept,
	 *                 false if the response is not acceptable and should be discarded
	 */
	public function isResponseAcceptable(RequestInterface $request, ResponseInterface $response) : bool;
	
}
