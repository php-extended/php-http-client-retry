<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-retry library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use DateTime;
use DateTimeImmutable;
use InvalidArgumentException;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * RetryClient class file.
 *
 * This class represents an http client that does retry requests on failure.
 *
 * @author Anastaszor
 */
class RetryClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner processor.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The configuration of this processor.
	 * 
	 * @var RetryConfiguration
	 */
	protected RetryConfiguration $_configuration;
	
	/**
	 * Builds a new CompressorProcessor with the given inner processor.
	 * 
	 * @param ClientInterface $client
	 * @param ?RetryConfiguration $configuration
	 */
	public function __construct(ClientInterface $client, ?RetryConfiguration $configuration = null)
	{
		$this->_client = $client;
		if(null === $configuration)
		{
			$configuration = new RetryConfiguration();
		}
		$this->_configuration = $configuration;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$retries = $this->_configuration->getNbRetries();
		$exceptions = [];

		do
		{
			try
			{
				$response = $this->_client->sendRequest($request);
				if($this->isResponseAcceptable($request, $response))
				{
					try
					{
						return $response->withAddedHeader('X-Retries-Left', "{$retries}");
					}
					catch(InvalidArgumentException $e)
					{
						return $response;
					}
				}
				
				$this->waitForRetryAfterHeader($response, 'X-Retry-After');
				$this->waitForRetryAfterHeader($response, 'Retry-After');
			}
			catch(ClientExceptionInterface $e)
			{
				$exceptions[] = $e;
			}
		}
		while(0 < $retries--);
		
		$message = 'Impossible to load url "{url}" (retry: {r})';
		$context = ['{url}' => $request->getUri()->__toString(), '{r}' => $this->_configuration->getNbRetries()];
		
		throw new RetryRequestException($request, $exceptions, \strtr($message, $context));
	}
	
	/**
	 * Gets whether the response is an acceptable response to the given 
	 * request.
	 * 
	 * @param RequestInterface $request
	 * @param ResponseInterface $response
	 * @return boolean true if the response should be accepted, false if it
	 *                 should be refused
	 */
	protected function isResponseAcceptable(RequestInterface $request, ResponseInterface $response) : bool
	{
		$callback = $this->_configuration->getAcceptabilityCallback();
		if(null !== $callback)
		{
			return $callback->isResponseAcceptable($request, $response);
		}
		
		$ret = $this->_configuration->isStatusCodeAcceptable($response->getStatusCode());
		if(!$ret)
		{
			return false;
		}
		
		if($this->_configuration->isEnabledRetryOnEmptyBody())
		{
			if(\in_array($request->getMethod(), ['GET', 'HEAD', 'OPTIONS', 'TRACE'], true)
				&& 0 === $response->getBody()->getSize()
			) {
				// if the content-length is set and is zero,
				// then the response is correctly formed
				// we should not flag it as unacceptable at this stage
				$contentLength = $response->getHeaderLine('Content-Length');
				if(!(\preg_match('#^\\d+$#ui', $contentLength) && 0 === ((int) $contentLength)))
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Waits for the numeric value in the given header of the response.
	 * 
	 * @param ResponseInterface $response
	 * @param string $header
	 */
	protected function waitForRetryAfterHeader(ResponseInterface $response, $header) : void
	{
		$retryAfter = $response->getHeader($header);
		
		foreach($retryAfter as $retryAfterValue)
		{
			if(\is_numeric($retryAfterValue))
			{
				$retryAfterValue = (int) $retryAfterValue;
				if(0 < $retryAfterValue)
				{
					\sleep(1 + \max(0, (int) $retryAfterValue));
				}
				continue;
			}
			
			// https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.37
			$dti = DateTimeImmutable::createFromFormat(DateTime::RFC1123, $retryAfterValue);
			if(false !== $dti)
			{
				$now = new DateTimeImmutable();
				$diff = $dti->getTimestamp() - $now->getTimestamp();
				if(0 < $diff)
				{
					\sleep(1 + \max(0, (int) $diff));
				}
			}
		}
	}
	
}
