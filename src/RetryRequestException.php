<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-retry library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\RequestExceptionInterface;
use Psr\Http\Message\RequestInterface;
use RuntimeException;
use Throwable;

/**
 * RetryRequestException class file.
 * 
 * This class represents an exception while doing the 
 * 
 * @author Anastaszor
 */
class RetryRequestException extends RuntimeException implements RequestExceptionInterface
{
	
	/**
	 * The original request.
	 * 
	 * @var RequestInterface
	 */
	protected RequestInterface $_request;
	
	/**
	 * The original exceptions.
	 * 
	 * @var array<integer, ClientExceptionInterface>
	 */
	protected array $_exceptions = [];
	
	/**
	 * Builds a new RetryRequestException with the given request and previous
	 * exceptions.
	 * 
	 * @param RequestInterface $request
	 * @param array<integer, ClientExceptionInterface> $exceptions
	 * @param string $message
	 * @param integer $code
	 * @param ?Throwable $previous
	 */
	public function __construct(RequestInterface $request, array $exceptions, string $message, int $code = -1, ?Throwable $previous = null)
	{
		$this->_request = $request;
		$this->_exceptions = $exceptions;
		if(null === $previous && !empty($this->_exceptions))
		{
			$previous = \end($this->_exceptions);
		}
		$messages = $message;
		
		foreach($this->_exceptions as $exception)
		{
			$messages .= "\n".$exception->getMessage();
		}
		
		parent::__construct($messages, $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\RequestExceptionInterface::getRequest()
	 */
	public function getRequest() : RequestInterface
	{
		return $this->_request;
	}
	
	/**
	 * Gets the original exceptions.
	 * 
	 * @return array<integer, ClientExceptionInterface>
	 */
	public function getExceptions() : array
	{
		return $this->_exceptions;
	}
	
}
