<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-retry library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\RetryConfiguration;
use PHPUnit\Framework\TestCase;

/**
 * RetryConfigurationTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\RetryConfiguration
 *
 * @internal
 *
 * @small
 */
class RetryConfigurationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var RetryConfiguration
	 */
	protected RetryConfiguration $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testNbRetries() : void
	{
		$this->assertEquals(4, $this->_object->getNbRetries());
		$this->_object->setNbRetries(12);
		$this->assertEquals(12, $this->_object->getNbRetries());
	}
	
	public function testHasRetryOnEmptyBodyEnabled() : void
	{
		$this->assertTrue($this->_object->isEnabledRetryOnEmptyBody());
		$this->_object->disableRetryGetOnEmptyBody();
		$this->assertFalse($this->_object->isEnabledRetryOnEmptyBody());
		$this->_object->enableRetryGetOnEmptyBody();
		$this->assertTrue($this->_object->isEnabledRetryOnEmptyBody());
	}
	
	public function testHasRetryCode() : void
	{
		$this->assertFalse($this->_object->hasRetryCode(100));
		$this->_object->addRetryCode(100);
		$this->assertTrue($this->_object->hasRetryCode(100));
		$this->_object->removeRetryCode(100);
		$this->assertFalse($this->_object->hasRetryCode(100));
	}
	
	public function testRetryCodes() : void
	{
		$this->assertEquals([408, 500, 502, 503, 504], $this->_object->getRetryCodes());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new RetryConfiguration();
	}
	
}
