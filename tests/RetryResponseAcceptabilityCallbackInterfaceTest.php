<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-retry library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\RetryResponseAcceptabilityCallbackInterface;
use PHPUnit\Framework\TestCase;

/**
 * RetryResponseAcceptabilityCallbackInterfaceTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\RetryResponseAcceptabilityCallbackInterface
 *
 * @internal
 *
 * @small
 */
class RetryResponseAcceptabilityCallbackInterfaceTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var RetryResponseAcceptabilityCallbackInterface
	 */
	protected RetryResponseAcceptabilityCallbackInterface $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->getMockForAbstractClass(RetryResponseAcceptabilityCallbackInterface::class);
	}
	
}
