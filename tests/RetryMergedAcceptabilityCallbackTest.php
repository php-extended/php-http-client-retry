<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-retry library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\RetryMergedAcceptabilityCallback;
use PhpExtended\HttpClient\RetryResponseAcceptabilityCallbackInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * RetryMergedAcceptabilityCallbackTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\RetryMergedAcceptabilityCallback
 *
 * @internal
 *
 * @small
 */
class RetryMergedAcceptabilityCallbackTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var RetryMergedAcceptabilityCallback
	 */
	protected RetryMergedAcceptabilityCallback $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testIsAcceptable() : void
	{
		$this->assertFalse($this->_object->isResponseAcceptable(
			$this->getMockForAbstractClass(RequestInterface::class),
			$this->getMockForAbstractClass(ResponseInterface::class),
		));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new RetryMergedAcceptabilityCallback(
			$this->getMockForAbstractClass(RetryResponseAcceptabilityCallbackInterface::class),
			$this->getMockForAbstractClass(RetryResponseAcceptabilityCallbackInterface::class),
		);
	}
	
}
