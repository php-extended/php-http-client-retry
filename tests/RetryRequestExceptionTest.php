<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-retry library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\RetryRequestException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;

/**
 * RetryRequestExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\RetryRequestException
 *
 * @internal
 *
 * @small
 */
class RetryRequestExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var RetryRequestException
	 */
	protected RetryRequestException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(RetryRequestException::class, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new RetryRequestException(
			$this->getMockForAbstractClass(RequestInterface::class),
			[],
			'message',
			-1,
			null,
		);
	}
	
}
